import axios from 'axios'

export const getTodoById = id => new Promise((resolve, reject) => {
  fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)
    .then(response => response.json())
    .then(data => resolve(data))
    .catch(error => reject(error))
})

export const getTodoAxios = async id => {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/todos/${id}`
  )
  return response.data
}