import App from '@/App'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import TodoList from '@/todo-components/TodoList'

import {shallowMount, enableAutoDestroy} from "@vue/test-utils"

enableAutoDestroy(afterEach)

// const TodoListStub = {
//   template: '<div>List</div>',
//   props: TodoList.props,
//   model: TodoList.model,
//   $_vueTestUtils_original: TodoList,
// }

// window.fetch = jest.fn(() => 
//   Promise.resolve({
//     json: () => Promise.resolve({
//       completed: false,
//       id: 1,
//       title: "Test",
//       userId: 1
//     })
//   })
// )


describe("App", () => {

  let wrapper, mock

  const createComponent = ({ props = {}, options = {} }) => {
    wrapper = shallowMount(App, {
      propsData: props,
      stubs: {
          TodoList
        },
        ...options
      })
    }
    beforeEach(() => {
      mock = new MockAdapter(axios)
    })

    afterEach(() => {
      fetch.resetMocks()
      mock.restore()
    })
    
    // it("Should change TodoList.vue props list length", async () => {
      //   fetch.mockResponse(JSON.stringify({
  //     completed: false,
  //     id: 1,
  //     title: "Test",
  //     userId: 1
  //   }))

  //   createComponent({})
  //   await wrapper.find('button').trigger('click')
  //   await flushPromises()
  //   expect(fetch).toHaveBeenLastCalledWith('https://jsonplaceholder.typicode.com/todos/1')
  //   expect(wrapper.findComponent(TodoList).props('list').length).toBe(1)
  // })

  it("Should change TodoList.vue props  listlength", async () => {
    mock.onGet('https://jsonplaceholder.typicode.com/todos/1').reply(200, {
      completed: false,
      id: 1,
      title: "Test",
      userId: 1
    })

    createComponent({})
    await wrapper.find('button').trigger('click')
    await flushPromises()
    expect(wrapper.findComponent(TodoList).props('list').length).toBe(1)
  })
})