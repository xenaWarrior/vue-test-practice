import CreateTodo from "@/todo-components/CreateTodo"

import { mount, enableAutoDestroy } from "@vue/test-utils";

enableAutoDestroy(afterEach)

describe('CreateTodo', () => {
  let wrapper
  
  const findBtnByText = text => wrapper.findAll('button').wrappers.find(w => w.text() === text)
  
  const createComponent = ({ props = {}, options = {} }) => {
    wrapper = mount(CreateTodo, {
      propsData: props,
      ...options
    })
  }

  it("Should call emit @create-todo on click button 'Create'", async () => {
    createComponent({})

    const inputTitle = wrapper.find('input[name="title"]')
    const inputProject = wrapper.find('input[name="project"]')
    await inputTitle.setValue('Title')
    await inputProject.setValue('Description')
    
    await findBtnByText('Create').trigger('click')

    expect(wrapper.emitted()['create-todo']).toBeTruthy()
  })

  it("Should close form on clicl button 'Cancel'", async () => {
    createComponent({})
    await findBtnByText('Cancel').trigger('click')
    expect(wrapper.find('.form').isVisible()).toBe(false)
  })
})