import TodoItem from "@/todo-components/TodoItem"

import {mount, enableAutoDestroy} from "@vue/test-utils"

enableAutoDestroy(afterEach)

describe('TodoItem Unit Test', () => {
  let wrapper
  
  const findBtnByText = text => wrapper.findAll('button').wrappers.find(w => w.text() === text)
  
  const createComponent = ({ props = {}, options = {} }) => {
    wrapper = mount(TodoItem, {
      propsData: props,
      ...options
    })
  }
  it("Should call emit '@complete-todo' on click button 'Complete'", async () => {
    const INITIAL_ITEM = {
      title: "Todo A",
      project: "Project A",
      completed: false,
      index: 0
    }
    createComponent({props: {todo: INITIAL_ITEM}})
    await findBtnByText('Complete Todo').trigger('click')
    expect(wrapper.emitted('complete-todo')).toBeTruthy()
  })

  it("Should appear completed label when Todo is completed", async () => {
    const INITIAL_ITEM = {
      title: "Todo A",
      project: "Project A",
      completed: true,
      index: 0
    }
    createComponent({props: {todo: INITIAL_ITEM}})
    expect(wrapper.find('.card__label--variant-success').isVisible()).toBe(true)
  })

  it("Should appear pending label when Todo isn't completed", async () => {
    createComponent({props: {todo: {}}})
    expect(wrapper.find('.card__label--variant-pending').isVisible()).toBe(true)
  })

  it("Should open todo inputs", async () => {
    createComponent({props: {todo: {}}})
    await findBtnByText('Edit').trigger('click')
    expect(wrapper.find('.card__edit').isVisible()).toBe(true)
  })

  it("Should call emit @save-changes on click button 'Save'", async () => {
    createComponent({props: {todo: {}}})
    await findBtnByText('Save').trigger('click')
    expect(wrapper.emitted()['save-changes']).toBeTruthy()
  })

  it("Shoulnd't call emit @save-changes on click button 'Close'", async () => {
    createComponent({props: {todo: {}}})
    await findBtnByText('Close').trigger('click')
    expect(wrapper.emitted()['save-changes']).not.toBeTruthy()
  })
})