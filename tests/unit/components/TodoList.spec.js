import TodoList from '@/todo-components/TodoList'
import TodoItem from '@/todo-components/TodoItem'

import {shallowMount, enableAutoDestroy} from "@vue/test-utils"

enableAutoDestroy(afterEach)

describe('TodoList', () => {
  let wrapper
  const createComponent = ({ props = {}, options = {} }) => {
    wrapper = shallowMount(TodoList, {
      propsData: props,
      ...options
    })
  }

  it("Should be prop completed: true on TodoItem", async () => {
    const INITIAL_LIST = [
      {
        title: "Todo A",
        project: "Project A",
        completed: true,
        index: 0
      }
    ]
    createComponent({ props: {list: INITIAL_LIST} })

    expect(wrapper.findComponent(TodoItem).props().todo.completed).toBe(true)
  })
})